package uk.ac.napier.gaelle1989.Gaia.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import uk.ac.napier.gaelle1989.Gaia.R;
//http://www.androidhive.info/2015/09/android-material-design-working-with-tabs/

public class EarringsFragment extends Fragment {

    public EarringsFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // materialise layout of the earrings fragment
        return inflater.inflate(R.layout.fragment_earrings, container, false);
    }

}