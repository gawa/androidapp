package uk.ac.napier.gaelle1989.Gaia;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import uk.ac.napier.gaelle1989.Gaia.Fragments.EarringsFragment;
import uk.ac.napier.gaelle1989.Gaia.Fragments.AllFragment;
import uk.ac.napier.gaelle1989.Gaia.Fragments.RingFragment;
import uk.ac.napier.gaelle1989.Gaia.Fragments.NecklacesFragment;

public class MainActivity extends AppCompatActivity {


    private static ArrayList<Jewellery> allJeweleryList = new ArrayList<Jewellery>();
    private static ArrayList<Jewellery> createCartList = new ArrayList<Jewellery>();

    public static ArrayList<Jewellery> getCreateCartList() {
        return createCartList;
    }


    private static Toolbar toolbar;
    private static TabLayout tabLayout;
    private static ViewPager viewPager;
    private int[] tabIcons = {
            R.drawable.ic_diamond,
            R.drawable.ic_necklace,
            R.drawable.ic_ring,
            R.drawable.ic_earrings
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);


        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();


        createJewellery();

    }




    private void setupTabIcons() {
        // TABS http://www.androidhive.info/2015/09/android-material-design-working-with-tabs/
        // gets item from icon list and sets its position within the tab bar

        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);
        tabLayout.getTabAt(3).setIcon(tabIcons[3]);
    }




    private void setupViewPager(ViewPager viewPager) {
        // VIEWPAGER http://developer.android.com/reference/android/support/v4/view/ViewPager.html
        // generates the page/fragment that the view shows

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());



        // adds each fragment to the page
        adapter.addFrag(new AllFragment(), getString(R.string.all));
        adapter.addFrag(new NecklacesFragment(), getString(R.string.necklaces));
        adapter.addFrag(new RingFragment(), getString(R.string.rings));
        adapter.addFrag(new EarringsFragment(), getString(R.string.earrings));
        viewPager.setAdapter(adapter);
    }


    class ViewPagerAdapter extends FragmentPagerAdapter {

        // makes list of fragments
        private final List<Fragment> myFragmentList = new ArrayList<>();
        private final List<String> myFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return myFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return myFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            myFragmentList.add(fragment);
            myFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }
    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //MENU inflates the menu option

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;

    }





    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // MENU selection of item in menu, gets item id from xml and creates toast when selected
        // When item is selected, create an new intend towards a new activity

        switch (item.getItemId()) {
            case R.id.home_id:
                Toast.makeText(getApplicationContext(), R.string.Home, Toast.LENGTH_LONG).show();
                return true;

            case R.id.about_id:
                Intent aboutIntent = new Intent(MainActivity.this, AboutActivity.class);
                startActivity(aboutIntent);
                Toast.makeText(getApplicationContext(), R.string.AboutGaia, Toast.LENGTH_LONG).show();
                return true;

            case R.id.cart_id:
                Intent cartIntent = new Intent(MainActivity.this, CartActivity.class);
                startActivity(cartIntent);
                Toast.makeText(getApplicationContext(), R.string.Cart, Toast.LENGTH_LONG).show();
                return true;

            case R.id.contact_id:
                Intent contactIntent = new Intent(MainActivity.this, ContactActivity.class);
                startActivity(contactIntent);
                Toast.makeText(getApplicationContext(), R.string.Contact, Toast.LENGTH_LONG).show();
                return true;






            default:
                return super.onOptionsItemSelected(item);
        }
    }



    public void imageClicked(View view) {
        // SEE EARRINGS fragment FOR IMAGE CLICKED FUNCTIONALITY
        // for each item of jewellery in jewelitem list, check for matching id from view
        //checks which one is selected from the list, its position in the list,Id and create item in cart


        for (int i = 0; i < allJeweleryList.size(); i++) {
            if (allJeweleryList.get(i).getID() == view.getId()) {
                createCartList.add(allJeweleryList.get(i));
                Toast.makeText(getBaseContext(), allJeweleryList.get((i)).getName() + " added to cart", Toast.LENGTH_LONG).show();
                break;
            }
        }
    }







    private void createJewellery() {
        // sets source for each object that will be added to the cart list

        Jewellery yellowEarring = new Jewellery(R.id.gold_earrings,
                getString(R.string.yellowearrings),
                getString(R.string.pricea),
                getResources().getDrawable(R.drawable.gold_earrings));
        allJeweleryList.add(yellowEarring);


        Jewellery turstuds = new Jewellery(R.id.turquoise_searrings,
                getString(R.string.turstuds),
                getString(R.string.priceb),
                getResources().getDrawable(R.drawable.tur_earrings));
        allJeweleryList.add(turstuds);


        Jewellery turqstuds = new Jewellery(R.id.turquoise_searrings,
                getString(R.string.turstuds),
                getString(R.string.priceb),
                getResources().getDrawable(R.drawable.tur_earrings));
        allJeweleryList.add(turqstuds);


        Jewellery moonearrings = new Jewellery(R.id.moon_earrings,
                getString(R.string.whiteearrings),
                getString(R.string.pricec),
                getResources().getDrawable(R.drawable.white_earrings));
        allJeweleryList.add(moonearrings);


        Jewellery trianglEarrings = new Jewellery(R.id.triangle_earrings,
                getString(R.string.turearring),
                getString(R.string.pricee),
                getResources().getDrawable(R.drawable.turq_earrings));
        allJeweleryList.add(trianglEarrings);


        Jewellery purpleEarrings = new Jewellery(R.id.purple_earrings,
                getString(R.string.purpleearring),
                getString(R.string.pricec),
                getResources().getDrawable(R.drawable.purple_earrings));
        allJeweleryList.add(purpleEarrings);


        Jewellery navyEarings = new Jewellery(R.id.lapis_earrings,
                getString(R.string.navyearring),
                getString(R.string.priceg),
                getResources().getDrawable(R.drawable.lapis_earrings));
        allJeweleryList.add(navyEarings);


    }
}
