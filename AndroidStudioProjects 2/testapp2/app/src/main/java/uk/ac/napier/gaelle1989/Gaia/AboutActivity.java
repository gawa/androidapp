package uk.ac.napier.gaelle1989.Gaia;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

public class AboutActivity extends AppCompatActivity {
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        TextView textAbout = (TextView) findViewById(R.id.about_gaia);
        TextView textLine1 = (TextView) findViewById(R.id.about_line1);
        TextView textLine2 = (TextView) findViewById(R.id.about_line2);
        TextView textLine3 = (TextView) findViewById(R.id.about_line3);
        TextView textLine4 = (TextView) findViewById(R.id.about_line4);
        TextView textLine5 = (TextView) findViewById(R.id.about_line5);
        TextView textLine6 = (TextView) findViewById(R.id.about_line6);
        TextView textLine7 = (TextView) findViewById(R.id.about_line7);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);//Menu Resource, Menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home_id:
                Intent homeIntent= new Intent(this,MainActivity.class);
                startActivity(homeIntent);
                Toast.makeText(getApplicationContext(), R.string.Home, Toast.LENGTH_LONG).show();
                return true;

            case R.id.about_id:
                Toast.makeText(getApplicationContext(),R.string.AboutGaia,Toast.LENGTH_LONG).show();
                return true;

            case R.id.cart_id:
                Intent cartIntent= new Intent(this,CartActivity.class);
                startActivity(cartIntent);
                Toast.makeText(getApplicationContext(),R.string.Cart,Toast.LENGTH_LONG).show();
                return true;

            case R.id.contact_id:
                Intent contactIntent= new Intent(this,ContactActivity.class);
                startActivity(contactIntent);
                Toast.makeText(getApplicationContext(),R.string.Contact,Toast.LENGTH_LONG).show();
                return true;






            default:
                return super.onOptionsItemSelected(item);
        }
    }



}