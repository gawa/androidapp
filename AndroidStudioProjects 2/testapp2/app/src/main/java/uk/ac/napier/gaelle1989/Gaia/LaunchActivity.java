package uk.ac.napier.gaelle1989.Gaia;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

public class LaunchActivity extends AppCompatActivity {
        private static ImageButton LaunchBtn;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_launch);
            OnClickButtonListener();
            Typeface gaiaType = Typeface.createFromAsset(getAssets(), "Moon Light.otf");
            TextView tx = (TextView)findViewById(R.id.app_name);
            tx.setTypeface(gaiaType);


        }



    public void OnClickButtonListener (){
        LaunchBtn = (ImageButton) findViewById(R.id.logogaia_id);
        LaunchBtn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(LaunchActivity.this,MainActivity.class);
                        startActivity(intent);
                    }
                }



        );
    }
    }