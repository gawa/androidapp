package uk.ac.napier.gaelle1989.Gaia;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

public class ContactActivity extends AppCompatActivity {
    private Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        Typeface gType = Typeface.createFromAsset(getAssets(), "Moon Light.otf");
        TextView gaiaTitle = (TextView)findViewById(R.id.app_name);
        gaiaTitle.setTypeface(gType);

        TextView textAdr = (TextView) findViewById(R.id.textAddress);
        TextView textPcode = (TextView) findViewById(R.id.textPcode);
        TextView textCit = (TextView) findViewById(R.id.textCity);
        TextView textPhone = (TextView) findViewById(R.id.textPhone);
        TextView textEmail = (TextView) findViewById(R.id.email_id);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu adds items on bar
        getMenuInflater().inflate(R.menu.menu_main, menu);//Menu Resource, Menu
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home_id:
                Intent homeIntent= new Intent(this,MainActivity.class);
                startActivity(homeIntent);
                Toast.makeText(getApplicationContext(), R.string.Home, Toast.LENGTH_LONG).show();
                return true;

            case R.id.about_id:
                Intent aboutIntent= new Intent(this,AboutActivity.class);
                startActivity(aboutIntent);
                Toast.makeText(getApplicationContext(),R.string.AboutGaia,Toast.LENGTH_LONG).show();
                return true;

            case R.id.cart_id:
                Intent cartIntent= new Intent(this,CartActivity.class);
                startActivity(cartIntent);
                Toast.makeText(getApplicationContext(),R.string.Cart,Toast.LENGTH_LONG).show();
                return true;

            case R.id.contact_id:
                Toast.makeText(getApplicationContext(),R.string.Contact,Toast.LENGTH_LONG).show();
                return true;



            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
