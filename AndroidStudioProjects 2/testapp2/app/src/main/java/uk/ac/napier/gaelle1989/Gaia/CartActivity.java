package uk.ac.napier.gaelle1989.Gaia;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;



public class CartActivity extends AppCompatActivity {

    private static ArrayList<Jewellery>myList;
    private static Toolbar toolbar;
    private static ListView mylistview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        myList = MainActivity.getCreateCartList();



        //adds items to list view
        CartAdapter mycartadapter = new CartAdapter(getBaseContext(),myList);
        mylistview = (ListView) findViewById(R.id.listView);
        mylistview.setAdapter(mycartadapter);


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate menu
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // finds source of text displayed, adds toast and intent towards new activity
        switch (item.getItemId()) {
            case R.id.home_id:
                Intent homeIntent = new Intent(this, MainActivity.class);
                startActivity(homeIntent);
                Toast.makeText(getApplicationContext(), R.string.Home, Toast.LENGTH_LONG).show();
                return true;

            case R.id.about_id:
                Intent aboutIntent= new Intent(this,AboutActivity.class);
                startActivity(aboutIntent);
                Toast.makeText(getApplicationContext(),R.string.AboutGaia,Toast.LENGTH_LONG).show();
                return true;

            case R.id.cart_id:
                Toast.makeText(getApplicationContext(),R.string.Cart,Toast.LENGTH_LONG).show();
                return true;

            case R.id.contact_id:
                Toast.makeText(getApplicationContext(),R.string.Contact,Toast.LENGTH_LONG).show();
                Intent contactIntent = new Intent(this, ContactActivity.class);
                startActivity(contactIntent);
                return true;



            default:
                return super.onOptionsItemSelected(item);
        }
    }
}


