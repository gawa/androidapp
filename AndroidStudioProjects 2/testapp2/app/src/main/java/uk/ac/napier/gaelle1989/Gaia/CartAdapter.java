package uk.ac.napier.gaelle1989.Gaia;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class CartAdapter extends BaseAdapter {
    //http://developer.android.com/reference/android/widget/Adapter.html
    // provides access to the data items and makes items viewable

    private static ArrayList<Jewellery> jewelleryList;
    private static LayoutInflater myInflater;

    public CartAdapter(Context c, ArrayList<Jewellery> myList)
    {
        jewelleryList = myList;
        // sets the local jewelitem within the class to the passed in jewelitem

        myInflater = LayoutInflater.from(c);
        //layout inflater used to direct the xml to the activity
    }
    @Override
    public View getView(int listPosition, View contextView, ViewGroup parent)
    {

        LinearLayout jewelLayout = (LinearLayout) myInflater.inflate(R.layout.jewelitem, parent, false);
        // creates layout from the inflater created in constructor



        ImageView myImage = (ImageView)jewelLayout.findViewById(R.id.myimage);
        TextView myPriceView = (TextView)jewelLayout.findViewById(R.id.price);
        TextView myNameView = (TextView)jewelLayout.findViewById(R.id.jewelname);
        //creates normal views of images and text



// set the source of the image and set the name text for the item of the jewelitem  passed in.
// create a local jewellery object, and set it to the chosen object from jewelitem.

        Jewellery currentPiece = jewelleryList.get(listPosition);
        // where jewelitem position is the parameter passed in

    //sets image source
        myImage.setImageDrawable(
                (Drawable)currentPiece.getDraw());

        myNameView.setText(currentPiece.getName());

        myPriceView.setText(currentPiece.getPrice());

        return jewelLayout;
        // return the view
        //called for each item in the list from activity.
    }

    @Override
    public int getCount() {
        return jewelleryList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

}



