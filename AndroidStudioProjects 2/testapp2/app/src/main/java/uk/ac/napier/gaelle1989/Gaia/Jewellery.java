package uk.ac.napier.gaelle1989.Gaia;

import android.graphics.drawable.Drawable;

/**
 * Created by Gaelle on 24/03/2016.
 */
public class Jewellery {

    private long id;
    private String name;
    private String price;
    private Drawable draw;

    public Jewellery (long myID, String myName, String myPrice, Drawable drawInput){
        id =myID;
        name=myName;
        price = myPrice;
        draw = drawInput;
    }

    public long getID()
    {
        return id;
    }


    public String getName (){

        return name;
    }

    public String getPrice(){

        return price;
    }

    public Drawable getDraw()
    {
        return draw;
    }
}
